import java.util.Date;

public class Card {
    private int id;
    private Visitor visitor;
    private Date date;
    private Date dateDue;

    public Card(int id, Visitor visitor, Date date, Date dateDue) {
        this.id = id;
        this.visitor = visitor;
        this.date = date;
        this.dateDue = dateDue;
        System.out.println("Посетителю выдана книга");
    }

    public void checkifOverdue() {
        System.out.println("Вызван метод checkifOverdue из класса Card");
    }

}