import java.util.Date;

public class Main {
  public static void main(String[] args) {
      System.out.println("\nДобавление книги:\n--------------------------------------------------");

      Library library = new Library();
      
      library.addNewBook(new Book("1984", 1949, "George Orwell", "A7"));

      System.out.println("--------------------------------------------------");
      System.out.println("Списание книги:\n--------------------------------------------------");

      library.searchBook();
      library.removeBook();

      System.out.println("Получение книги:\n--------------------------------------------------");

      Visitor visitor = new Visitor("Bogdan", "8800553535");

      library.findVisitor();
      library.searchBook();

      book.changePresenceStatus();
      book.requestPosition();
      book.addNewCard(visitor);

      Date current_date = new Date();
      Date dueDate = new Date(current_date.getTime() + 1209600000);
      Card card = new Card(0, visitor, current_date, dueDate);
      System.out.println("Получена позиция книги");

      System.out.println("--------------------------------------------------");
      System.out.println("Возвращение книги:\n--------------------------------------------------");

      library.returnBook();
      card.checkifOverdue();
      library.fineVisitor();
      book.changePresenceStatus();
      book.requestPosition();
      System.out.println("Получена позиция книги\n");
    }
}

