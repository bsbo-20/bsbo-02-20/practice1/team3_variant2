public class Book {
    private String name;
    private int year;
    private String author;
    private String position;
    private Card[] cards;
    private boolean isPresent;

    public Book(String name, int year, String author, String position) {
        this.name = name;
        this.year = year;
        this.author = author;
        this.position = position;
        this.cards = new Card[10];
        this.isPresent = true;
        System.out.println("Добавлена новая книга");
    }

    public void changePresenceStatus() {
        System.out.println("Вызван метод changePresenceStatus из класса Book");
    }

    public void requestPosition() {
        System.out.println("Вызван метод requestPosition из класса Book");
    }

    public void addNewCard(Visitor visitor) {
        System.out.println("Вызван метод addNewCard из класса Book");
    }

}