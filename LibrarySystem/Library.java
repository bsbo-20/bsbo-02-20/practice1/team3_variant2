public class Library {
    private Book[] books;
    private Visitor[] visitors;

    public Library() {
        this.books = new Book[1000];
        this.visitors = new Visitor[100];
    }

    public void searchBook() {
        System.out.println("Вызван метод searchBook из класса Library");
    }

    public void addNewBook(Book book) {
        System.out.println("Вызван метод addNewBook из класса Library");
    }

    public void findFreePosition() {
        System.out.println("Вызван метод findFreePosition из класса Library");
    }

    public void removeBook() {
        System.out.println("Вызван метод removeBook из класса Library");
    }

    public void giveBook() {
        System.out.println("Вызван метод giveBook из класса Library");
    }

    public void returnBook() {
        System.out.println("Вызван метод returnBook из класса Library");
    }

    public void fineVisitor() {
        System.out.println("Вызван метод fineVisitor из класса Library");
    }

    public void removeFine() {
        System.out.println("Вызван метод removeFine из класса Library");
    }

    public void findVisitor() {
        System.out.println("Вызван метод findVisitor из класса Library");
    }
}
