public class Visitor {
    private String name;
    private String contact;
    private int fineSum;

    public Visitor(String name, String contact) {
        this.name = name;
        this.contact = contact;
        this.fineSum = 0;
        System.out.println("Добавлен новый посетитель");
    }

    public void fine() {
        System.out.println("Вызван метод fine из класса Visitor");
    }

    public void removeFine() {
        System.out.println("Вызван метод removeFine из класса Visitor");
    }

}